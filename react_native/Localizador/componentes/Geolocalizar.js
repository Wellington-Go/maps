import React,{useState} from 'react';
import {View,Text,TouchableHighlight} from 'react-native';
import Local from '@react-native-community/geolocation';


export default function(){

    const [lat,setLatitude]=useState(0)
    const [lon,setLongitude]=useState(0)

    const obterLocal = ()=>{
        Local.getCurrentPosition(
            (pos)=>{
                setLatitude(pos.coords.latitude)
                setLongitude(pos.coords.longitude)
            },
            (erro)=>{
                alert('erro' + erro.message)
            },
            {
                enableHighAccuracy:true,timeout:120000,maximumAge:1000
            }
            
        )
    }


  return (
        <View>
          <Text>Geolocalizador</Text>
          <TouchableHighlight onPress={obterLocal}>
              <Text>click aqui</Text>
          </TouchableHighlight>
          <Text>Latitude: {lat}</Text>
          <Text>longitude: {lon}</Text>
        </View>
  );
};

